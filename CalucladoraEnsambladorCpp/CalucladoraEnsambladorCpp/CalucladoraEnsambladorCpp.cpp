#include <iostream>
using namespace std;

int main()
{
	char b = ' ';
	int s = 0;
	short num1 = 0, num2 = 0;
	float n1 = 0, n2 = 0;
	short r = 0, r2;
	double t = 0, pi = 3.14159265, c = 180;
	bool ver = false;
	while (b != 'n')
	{
		cout << "Ingersa un numero de a lista" << endl << endl;

		cout << "1) --- SUMA" << endl;
		cout << "2) --- RESTA" << endl;
		cout << "3) --- MULTIPLICACION" << endl;
		cout << "4) --- DIVISION" << endl;
		cout << "5) --- SENO" << endl;
		cout << "6) --- COSENO" << endl;
		cout << "7) --- TANGENTE" << endl;
		cout << "8) --- POTENCIA" << endl;
		cout << "9) --- RAIZ CUADRADA" << endl;

		cin >> s;

		switch (s)
		{
		case 1:
			cout << endl << "Se a seleccionado la opcion 'SUMA'" << endl;
			cout << "Ingersa el dato 1:  ";
			cin >> num1;
			cout << "Ingersa el dato 2:  ";
			cin >> num2;
			_asm
			{
				mov ax, num1;
				mov bx, num2;
				add ax, bx;
				mov r, ax;
			}
			printf("El resulado es:  %d\n", r);
			break;
		case 2:
			cout << endl << "Se a seleccionado la opcion 'RESTA'" << endl;
			cout << "Ingersa el dato 1:  ";
			cin >> num1;
			cout << "Ingersa el dato 2:  ";
			cin >> num2;
			_asm
			{
				mov ax, num1;
				mov bx, num2;
				sub ax, bx;
				mov r, ax;
			}
			printf("El resulado es:  %d\n", r);
			break;
		case 3:
			cout << endl << "Se a seleccionado la opcion 'MULTIPLICACION'" << endl;
			cout << "Ingersa el dato 1:  ";
			cin >> num1;
			cout << "Ingersa el dato 2:  ";
			cin >> num2;
			_asm
			{
				mov ax, num1;
				mov bx, num2;
				mul bx;
				mov r, ax;
			}
			printf("El resulado es:  %d\n", r);
			break;
		case 4:
			cout << endl << "Se a seleccionado la opcion 'DIVISION'" << endl;
			cout << "Ingersa el dato 1:  ";
			cin >> num1;
			cout << "Ingersa el dato 2:  ";
			cin >> num2;
			_asm
			{
				xor dx, dx;
				mov ax, num1;
				mov bx, num2;
				div bx;
				mov r, ax;
			}
			printf("El resulado es:  %d\n", r);
			break;
		case 5:
			cout << endl << "Se a seleccionado la opcion 'SENO'" << endl;
			cout << "Ingersa el grado a medir:  ";
			cin >> num1;
			_asm
			{
				xor dx, dx;
				mov ax, num1;
				mov r, ax;
				fldpi;
				mov bx, num1;
				fsin;
				mov bx, r2;
			}
			t = sin(r * pi / c);
			printf("El resulado es:  %.4f\n", t);
			break;
		case 6:
			cout << endl << "Se a seleccionado la opcion 'COSENO'" << endl;
			cout << "Ingersa el grado a medir:  ";
			cin >> num1;
			_asm
			{
				mov ax, num1;
				mov r, ax;
				fldpi;
				mov bx, num1;
				fcos;
				mov bx, r2;
			}
			t = cos(r * pi / c);
			printf("El resulado es:  %.4f\n", t);
			break;
		case 7:
			cout << endl << "Se a seleccionado la opcion 'TENGENTE'" << endl;
			cout << "Ingersa el grado a medir:  ";
			cin >> num1;
			_asm
			{
				mov ax, num1;
				mov r, ax;
				fldpi;
				mov bx, num1;
				fsin;
				mov bx, num1;
				fcos;
				mov cx, bx;
				mov r2, cx;
			}
			t = tan(r * pi / c);
			printf("El resulado es:  %.4f\n", t);
			break;
		case 8:
			cout << endl << "Se a seleccionado la opcion 'POTENCIA'" << endl;
			cout << "Ingersa el dato 1:  ";
			cin >> num1;
			cout << "Ingersa la potencia:  ";
			cin >> num2;
			_asm
			{
				mov ax, num1;
				mov bx, num2;
				pop bx;
				mov r2, ax;
			}
			r = pow(num1, num2);
			printf("El resulado es:  %d\n", r);
			break;
		case 9:
			cout << endl << "Se a seleccionado la opcion 'RAIZ'" << endl;
			cout << "Ingersa el dato 1:  ";
			cin >> num1;
			_asm
			{
				mov ax, num1;
				mov r, ax;
				pop bx;
				mov r, ax;
			}
			r = sqrt(num1);
			printf("El resulado es:  %d\n", r);
			break;
		default:
			cout << "Eso no existe" << endl;
			break;
		}

		r = num1 = num2 = 0;
		cout << "Quieres continuar (s/n):  ";
		cin >> b;
		while (ver != true)
		{
			if ((b == 's') || (b == 'n'))
				ver = true;
			else {
				cout << "No existe esa vairables vuelve a intentar ...\nESTA CLARO \n:<";
				cout << "\nQuieres continuar (s/n):  ";
				cin >> b;
			}
		}

	}
	cout << "Adios";
	return 0;
}
